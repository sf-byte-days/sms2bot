package com.sms2bot;

import java.util.Locale;

import static com.sms2bot.OSUtils.HostOS.Linux;
import static com.sms2bot.OSUtils.HostOS.MacOS;
import static com.sms2bot.OSUtils.HostOS.Unknown;
import static com.sms2bot.OSUtils.HostOS.Windows;

public class OSUtils {

    public enum HostOS {
        Windows, MacOS, Linux, Unknown
    }

    ;

    public static HostOS detectHostOS() {

        HostOS hostOS = HostOS.Unknown;
        String detectedHostOS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((detectedHostOS.indexOf("mac") >= 0) || (detectedHostOS.indexOf("darwin") >= 0)) {
            hostOS = MacOS;
        } else if (detectedHostOS.indexOf("win") >= 0) {
            hostOS = Windows;
        } else if (detectedHostOS.indexOf("nux") >= 0) {
            hostOS = Linux;
        } else {
            hostOS = Unknown;
        }

        return hostOS;
    }

}
