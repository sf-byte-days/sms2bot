package com.sms2bot.sms;

import static com.google.common.base.MoreObjects.toStringHelper;

public class SmsMessage {

    private String destination;
    private String text;

    public SmsMessage() {
        this("N/A", "N/A");
    }

    public SmsMessage(String destination, String text) {
        this.destination = destination;
        this.text = text;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return toStringHelper("SMSMessage")
                .add("destination", destination)
                .add("text", text)
                .toString();
    }

}
