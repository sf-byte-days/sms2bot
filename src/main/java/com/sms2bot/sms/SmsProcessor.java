package com.sms2bot.sms;

import com.sms2bot.bot.BotMessageManager;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.*;
import org.smslib.Message.MessageTypes;

public class SmsProcessor implements IInboundMessageNotification {

    public static Logger LOG = LoggerFactory.getLogger(SmsProcessor.class);

    @VisibleForTesting
    BotMessageManager bot;

    public SmsProcessor(BotMessageManager bot) {
        this.bot = bot;
    }

    public void process(AGateway gateway, MessageTypes msgType, InboundMessage msg) {

        try {
            LOG.info("SMS {} received. Redirecting to the bot...");
            for (String channel : bot.getSubscribedChannels()) {
                switch (msgType) {
                    case STATUSREPORT:
                        StatusReportMessage statusMessage = (StatusReportMessage) msg;
                        bot.sendMessage(channel, "Received delivery notification for message with key ["
                                + statusMessage.getRecipient() + "-" + statusMessage.getRefNo() +
                                "]. Status is : [" + statusMessage.getStatus() + "]");
                        break;
                    default:
                        bot.sendMessage(channel, "Received SMS : [" + msg.getText() + "] from number [" + msg.getOriginator() + "]");
                        break;
                }
            }
            Service.getInstance().deleteMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
