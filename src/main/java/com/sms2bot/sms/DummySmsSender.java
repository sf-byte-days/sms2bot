package com.sms2bot.sms;

import com.google.common.util.concurrent.AbstractIdleService;
import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.processors.SmsSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class DummySmsSender extends AbstractIdleService implements SmsSender {

    private static final Logger LOG = LoggerFactory.getLogger(DummySmsSender.class);

    private BotMessageManager bot;

    @Inject
    public DummySmsSender(BotMessageManager bot) throws Exception {
        this.bot = bot;
        LOG.info("Dummy SMS sender created");
    }

    @Override
    protected void startUp() throws Exception {
        LOG.info("Dummy SMS sender started");
    }

    @Override
    protected void shutDown() throws Exception {
        LOG.info("Dummy SMS sender stopped");
    }

    @Override
    public String sendSms(String origin, String destination, String text) throws Exception {
        bot.sendMessage("dummy_channel", text);
        LOG.info("Dummy SMS {} sent to {}", text, destination);
        return "dummyId";
    }

}
