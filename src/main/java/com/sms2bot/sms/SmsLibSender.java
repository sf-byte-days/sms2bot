package com.sms2bot.sms;

import com.sms2bot.processors.SmsSender;
import com.google.common.util.concurrent.AbstractIdleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.ModemGateway;

import javax.inject.Inject;

import static com.google.common.base.MoreObjects.toStringHelper;

public class SmsLibSender extends AbstractIdleService implements SmsSender {

    private static final Logger LOG = LoggerFactory.getLogger(SmsLibSender.class);

    private Service smsLibService;

    @Inject
    public SmsLibSender(Service smsLibService) throws Exception {
        this.smsLibService = smsLibService;
    }

    @Override
    protected void startUp() throws Exception {
        smsLibService.startService();
        // This is very dirty but its necessary if we want to print the info of the gateway
        ModemGateway modemGateway = (ModemGateway) smsLibService.getGateways().iterator().next();
        LOG.info(getGatewayInfoAsString(modemGateway));
    }

    @Override
    protected void shutDown() throws Exception {
        LOG.debug("Called shutdown");
        smsLibService.stopService();
    }

    @Override
    public String sendSms(String origin, String destination, String text) throws Exception {
        OutboundMessage smsToSend = new OutboundMessage(destination, text);
        smsToSend.setStatusReport(true);
        smsLibService.sendMessage(smsToSend);
        LOG.info("Message [{}] sent", smsToSend);
        return smsToSend.getRecipient() + "-" + smsToSend.getRefNo();

    }

    static String getGatewayInfoAsString(ModemGateway gateway) throws Exception {

        return toStringHelper("Modem Information")
                .add("Manufacturer", gateway.getManufacturer())
                .add("Model", gateway.getModel())
                .add("Serial No", gateway.getSerialNo())
                .add("SIM IMSI", gateway.getImsi())
                .add("Signal Level (dBm)", gateway.getSignalLevel())
                .add("Battery Level (%)", gateway.getBatteryLevel())
                .toString();

    }

}
