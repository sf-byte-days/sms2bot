package com.sms2bot.processors;

import com.google.common.util.concurrent.Service;

public interface SmsSender extends Service {

    /**
     * This method will send an SMS to the number indicated as destination.
     * @param origin
     * @param destination
     * @param message
     * @return A String with an identifier of the message sent. It will be used to match later if  delivery notification is received.
     * @throws Exception
     */
    String sendSms(String origin, String destination, String message) throws Exception;

}
