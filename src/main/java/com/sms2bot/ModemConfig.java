package com.sms2bot;

import com.sms2bot.utils.YAMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModemConfig {

    private static final Logger LOG = LoggerFactory.getLogger(ModemConfig.class);

    public static final String MODEM_ID = "USModem";
    public static final String MODEM_BRAND = "Huawei";
    public static final String MODEM_MODEL = "E173";

    private String modemComPort;
    private int modemBaudRate;
    private String simPin;

    private static final String DEFAULT_CONFIG_FILE_NAME = "conf/default-modem.yml";

    public ModemConfig(String configFileName) {
        YAMLUtils.loadSettings(configFileName, DEFAULT_CONFIG_FILE_NAME, this);
    }

    public String getSimPin() {
        return simPin;
    }

    public void setSimPin(String simPin) {
        this.simPin = simPin;
    }

    public String getModemComPort() {
        return modemComPort;
    }

    public void setModemComPort(String modemComPort) {
        this.modemComPort = modemComPort;
    }

    public int getModemBaudRate() {
        return modemBaudRate;
    }

    public void setModemBaudRate(int modemBaudRate) {
        this.modemBaudRate = modemBaudRate;
    }

    @Override
    public String toString() {
        return "ModemConfig{" +
                "modemComPort='" + modemComPort + '\'' +
                ", modemBaudRate=" + modemBaudRate +
                ", simPin='" + simPin + '\'' +
                '}';
    }
}
