package com.sms2bot;

import com.sms2bot.sms.DummySmsSender;
import com.sms2bot.utils.ConfigUtils.BotCommand;
import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.bot.DummyBotMessageManager;
import com.sms2bot.bot.slack.SlackBotCaller;
import com.sms2bot.bot.telegram.TelegramBotCaller;
import com.sms2bot.processors.SmsSender;
import com.sms2bot.sms.SmsLibSender;
import com.sms2bot.sms.SmsProcessor;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.sms2bot.utils.Slack;
import com.sms2bot.utils.Telegram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.GatewayException;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageManagerModule extends AbstractModule {

    private static final Logger LOG = LoggerFactory.getLogger(MessageManagerModule.class);

    private final List<String> portsUsed = new ArrayList<>();

    private BotCommand botConfig;

    public MessageManagerModule(BotCommand botCommand) {

    }

    public MessageManagerModule(Map<String, Object> commands) {
        botConfig = (BotCommand) commands.get("bot");
    }

    @Override
    protected void configure() {

        switch(botConfig.type) {
            case TELEGRAM:
                bind(SmsSender.class).to(SmsLibSender.class).asEagerSingleton();
                bind(BotMessageManager.class).to(TelegramBotCaller.class).asEagerSingleton();
                break;
            case SLACK:
                bind(SmsSender.class).to(SmsLibSender.class).asEagerSingleton();
                bind(BotMessageManager.class).to(SlackBotCaller.class).asEagerSingleton();
                break;
            case DUMMY:
                bind(SmsSender.class).to(DummySmsSender.class).asEagerSingleton();
                bind(BotMessageManager.class).to(DummyBotMessageManager.class).asEagerSingleton();
                break;
        }

    }

    @Provides
    Service getSMSLibService(BotMessageManager bot) throws GatewayException {

        Service smsLibService = Service.getInstance();
        ModemConfig config;
        switch (OSUtils.detectHostOS()) {
            case MacOS:
            case Linux:
                config = new ModemConfig("conf/modem-linux.yml");
                // Next 2 lines are needed if working with rxtx (Linux)
                smsLibService.S.SERIAL_NOFLUSH = true;
                smsLibService.S.SERIAL_POLLING = true;
                break;
            case Windows:
                config = new ModemConfig("conf/modem-windows.yml");
                break;
            default:
                throw new IllegalStateException("OS not supported: " + OSUtils.detectHostOS());
        }

        SerialModemGateway gateway = new SerialModemGateway(ModemConfig.MODEM_ID,
                config.getModemComPort(),
                config.getModemBaudRate(),
                ModemConfig.MODEM_BRAND,
                ModemConfig.MODEM_MODEL);
        gateway.setSimPin(config.getSimPin());
        gateway.setInbound(true);
        gateway.setOutbound(true);
        LOG.info("Added gateway " + gateway);
        smsLibService.addGateway(gateway);
        smsLibService.setInboundMessageNotification(new SmsProcessor(bot));
        portsUsed.add(config.getModemComPort());

        return smsLibService;

    }

    @Provides @Slack
    String getSlackToken() {
        return getToken("conf/slack.yml");
    }

    @Provides @Telegram
    String getTelegramToken() {
        return getToken("conf/telegram.yml");
    }

    String getToken(String confFileLocation) {
        BotConfig config = new BotConfig(confFileLocation);
        return config.getToken();
    }

}
