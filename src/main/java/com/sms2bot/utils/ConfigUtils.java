package com.sms2bot.utils;

import com.beust.jcommander.*;
import com.sms2bot.bot.Bot;

import java.util.Map;

public class ConfigUtils {

    public static void parseArgs(String args[], Map<String, Object> commands) {

        JCommander commander = new JCommander();

        commands.forEach((key, value) -> {
            commander.addCommand(key, value);
        });

        try {
            commander.parse(args);
        } catch (ParameterException ex) {
            commander.usage();
            throw new IllegalArgumentException(ex.getMessage());
        }

    }

    @Parameters(commandDescription = "Bot configuration")
    public static class BotCommand {

        @Parameter(names = "-type", description = "Bot type (telegram, slack, dummy)", converter = BotConverter.class)
        public Bot type = Bot.DUMMY;

        @Parameter(names = "-token")
        public String token;

    }

    public static  class BotConverter implements IStringConverter<Bot> {

        @Override
        public Bot convert(String value) {
            Bot convertedValue = Bot.fromString(value);

            if(convertedValue == null) {
                throw new ParameterException("Value " + value + " can not be converted to a bot type. " +
                        "Available values are: telegram, slack, dummy.");
            }
            return convertedValue;
        }

    }

}
