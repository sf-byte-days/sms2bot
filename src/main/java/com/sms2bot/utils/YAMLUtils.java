package com.sms2bot.utils;

import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class YAMLUtils {

    private static final Logger LOG = LoggerFactory.getLogger(YAMLUtils.class);

    public static void loadSettings(String resourcePath, String defaultResourcePath, Object bean) {
        try {
            Map properties = loadSettings(resourcePath, defaultResourcePath);
            BeanUtils.populate(bean, properties);
        } catch (IllegalAccessException | InvocationTargetException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private static Map loadSettings(String resourcePath, String defaultResourcePath) throws IOException {
        Map defaultSetting = loadAsMap(defaultResourcePath);
        Preconditions.checkState(defaultSetting.size() > 0, String.format("Failed to load file '%s' from classpath", defaultResourcePath));
        if (resourcePath != null) {
            Map userSetting = loadAsMap(resourcePath);
            defaultSetting.putAll(userSetting);
        }
        return defaultSetting;
    }

    @SuppressWarnings("unchecked")
    private static Map loadAsMap(String path) throws IOException {
        try {
            String content = Resources.toString(Resources.getResource(path), Charset.forName("UTF-8"));
            LOG.debug("Loaded resource file '{}'\n{}", path, content);
            Map settings = new Yaml().loadAs(content, Map.class);
            if (settings == null) {
                settings = new HashMap(0);
            }
            return settings;
        } catch (IllegalArgumentException e) {
            return new HashMap();
        }
    }

}

