package com.sms2bot.utils;

import com.google.common.base.CharMatcher;

import static com.google.common.base.MoreObjects.toStringHelper;

public class MiscUtils {

    public static boolean isValid(String destinationNumber) {
        if (destinationNumber == null || destinationNumber.length() < 3) {
            return false;
        }
        // Number can be a shortcode or a mobile phone number
        return isShortCode(destinationNumber) || isMobilePhone(destinationNumber);
    }

    // Short codes are phone special numbers. See https://en.wikipedia.org/wiki/Short_code
    public static boolean isShortCode(String destinationNumber) {
        if(destinationNumber == null || destinationNumber.length() < 3 || destinationNumber.length() > 8) {
            return false;
        }
        return CharMatcher.javaDigit().matchesAllOf(destinationNumber);
    }

    public static boolean isMobilePhone(String destinationNumber) {
        if (destinationNumber == null || destinationNumber.length() < 9 || destinationNumber.length() > 20) {
            return false;
        }
        // International numbers can start with '+'
        return CharMatcher.is('+').or(CharMatcher.digit()).matches(destinationNumber.charAt(0)) &&
                CharMatcher.digit().matchesAllOf(destinationNumber.substring(1));
    }

}