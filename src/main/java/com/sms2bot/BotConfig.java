package com.sms2bot;

import com.sms2bot.utils.YAMLUtils;

class BotConfig {

    private String token;

    BotConfig(String configFileName) {
        YAMLUtils.loadSettings(configFileName, "conf/default-bot.yml", this);
    }

    String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
