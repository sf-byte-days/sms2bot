package com.sms2bot;

import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.processors.SmsSender;
import com.sms2bot.sms.SmsMessage;
import com.sms2bot.utils.ConfigUtils;
import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MessageManager extends AbstractExecutionThreadService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageManager.class);

    @Inject private BotMessageManager bot;
    @Inject private SmsSender smsService;

    public static void main(String[] args) throws Exception {

        ConfigUtils.BotCommand botCommand = new ConfigUtils.BotCommand();
        Map<String, Object> commands = new HashMap<>();
        commands.put("bot", botCommand);
        ConfigUtils.parseArgs(args, commands);

        // For injecting dummies use DummyModule
        Injector injector = Guice.createInjector(new MessageManagerModule(commands));
        MessageManager manager = injector.getInstance(MessageManager.class);
        manager.attachShutDownHook();
        manager.startAsync();
        manager.awaitTerminated();

    }

    @Override
    protected void startUp() throws Exception {
        smsService.startAsync();
        smsService.awaitRunning();
    }

    @Override
    protected void run() throws Exception {
        while(isRunning()) {
            if (bot.requiresPolling()) {
                LOG.info("Polling!");
                polling();
            }
        }
        LOG.info("Exiting...");
    }

    @Override
    protected void shutDown() throws Exception {

        smsService.stopAsync();
        smsService.awaitTerminated();

    }

    private void attachShutDownHook() {

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {

                stopAsync();
                awaitTerminated();

            }
        });
        LOG.info("Shutdown Hook Attached. You can stop me now with CTRL+C !!!");
    }

    private void polling() throws Exception {

        for (SmsMessage msg : bot.getMessages()) {
            smsService.sendSms("origin", msg.getDestination(), msg.getText());
        }
        LOG.info("Waiting 2 secs before polling again...");
        TimeUnit.SECONDS.sleep(2);

    }

}
