package com.sms2bot.bot.telegram;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.google.common.base.MoreObjects.toStringHelper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdatesResponse {

    private boolean ok;
    @JsonProperty("result")
    private List<Update> updates;

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public List<Update> getUpdates() {
        return updates;
    }

    public void setUpdates(List<Update> updates) {
        this.updates = updates;
    }

    @Override
    public String toString() {

        return toStringHelper("UpdatesResponse")
                .add("isOK", ok)
                .add("updates", updates)
                .toString();

    }


}
