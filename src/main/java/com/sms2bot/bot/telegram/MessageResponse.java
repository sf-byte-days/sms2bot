package com.sms2bot.bot.telegram;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.google.common.base.MoreObjects.toStringHelper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResponse {

    private boolean ok;
    @JsonProperty("result")
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessages(Message message) {
        this.message = message;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @Override
    public String toString() {

        return toStringHelper("MessageResponse")
                .add("isOK", ok)
                .add("message", message)
                .toString();

    }

}
