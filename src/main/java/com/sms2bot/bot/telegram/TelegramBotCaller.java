package com.sms2bot.bot.telegram;

import com.google.inject.Inject;
import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.sms.SmsMessage;
import com.google.common.annotations.VisibleForTesting;
import com.sms2bot.utils.Telegram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.sms2bot.utils.MiscUtils.isValid;

// TODO Test most used or critical path in here
public class TelegramBotCaller implements BotMessageManager {

    public static final String TELEGRAM_BASE_URL = "https://api.telegram.org/";
    public static final String GET_UPDATES_PATH = "/getupdates";
    public static final String SEND_MESSAGE_PATH = "/sendmessage";
    public static Logger LOG = LoggerFactory.getLogger(TelegramBotCaller.class);

    private final String getUpdatesURL;
    private final String sendMessageURL;
    private final String token;

    private Set<String> listeners = new HashSet<>();
    private long lastProcessedMessage = 0L;

    @Inject
    public TelegramBotCaller(@Telegram String token) throws Exception {

        this.token = token;
        this.getUpdatesURL = TELEGRAM_BASE_URL + token + GET_UPDATES_PATH;
        this.sendMessageURL = "https://api.telegram.org/" + token + SEND_MESSAGE_PATH;

    }

    @Override
    public boolean requiresPolling() {
        return true;
    }

    @Override
    public List<String> getSubscribedChannels() {
        return new ArrayList<>(listeners);
    }

    @Override
    public List<SmsMessage> getMessages() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getUpdatesURL)
                .queryParam("offset", lastProcessedMessage);
        UpdatesResponse getUpdateResponse = restTemplate.getForObject(builder.build()
                .encode().toUri(), UpdatesResponse.class);
        updateLastProcessedMessage(getUpdateResponse);
        LOG.info("{}", getUpdateResponse);
        return handleTelegramUpdates(getUpdateResponse);
    }

    @Override
    public void sendMessage(String channel, String text) {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(sendMessageURL).queryParam("chat_id",
                channel).queryParam("text", text);
        MessageResponse sendMessageResponse = restTemplate.getForObject(builder.build().encode().toUri(),
                MessageResponse.class);
        LOG.info("{}", sendMessageResponse);
    }

    private void updateLastProcessedMessage(UpdatesResponse getUpdateResponse) {
        ArrayList<Update> updates = new ArrayList<>(getUpdateResponse.getUpdates());
        LOG.info("{}", updates);
        if (updates.size() != 0) {
            Update update = updates.get(updates.size() - 1);
            lastProcessedMessage = update.getId() + 1;
        }
    }

    @VisibleForTesting
    List<SmsMessage> handleTelegramUpdates(UpdatesResponse updateResponse) throws Exception {
        if (updateResponse == null || updateResponse.getUpdates() == null) {
            return Collections.emptyList();
        }
        List<SmsMessage> smsMessageList = new ArrayList<>();
        for (Update update : updateResponse.getUpdates()) {
            String chat = update.getMessage().getChat().getId();
            String text = update.getMessage().getText();
            String[] tokens = text.split(" ");
            switch (tokens[0]) {
                case "startListening":
                    startListening(chat);
                    break;
                case "stop":
                    stopListening(chat);
                    break;
                case "send":
                    if (tokens.length < 3) {
                        sendMessage(chat, "For sending an sms, please use: send <number> <message>");
                        continue;
                    }
                    String destinationNumber = tokens[1];
                    if (!isValid(destinationNumber)) {
                        sendMessage(chat, "Please enter a valid destination number [" + destinationNumber + "]");
                        continue;
                    }
                    String textToSend = text.substring(text.indexOf(tokens[2]));

                    SmsMessage smsMessage = new SmsMessage();
                    smsMessage.setDestination(destinationNumber);
                    smsMessage.setText(textToSend);
                    smsMessageList.add(smsMessage);
                    sendMessage(chat, "Sending text message : [" + textToSend + "] to number: [" + destinationNumber + "]");
                    break;
                default:
                    sendMessage(update.getMessage().getChat().getId(), "Please use operations send, startListening, or stop.");
                    break;
            }
        }
        return smsMessageList;
    }

    private void startListening(String chatId) {
        listeners.add(chatId);
        LOG.info("Listening channel {}. Channels: {}", chatId, listeners);
    }

    private void stopListening(String chatId) {
        listeners.remove(chatId);
        LOG.info("Stopping listening {}. Channels: {}", chatId, listeners);
    }

}