package com.sms2bot.bot.slack;

import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.processors.SmsSender;
import com.sms2bot.sms.SmsMessage;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.sms2bot.utils.Slack;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import org.apache.http.MethodNotSupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.sms2bot.utils.MiscUtils.isValid;

// TODO Test most used or critical path in here
public class SlackBotCaller implements BotMessageManager, SlackMessagePostedListener, Closeable {

    private static Logger LOG = LoggerFactory.getLogger(SlackBotCaller.class);

    private static final String BOT_NAME = "smsbot";
    private static final String CHANNEL_NAME = "sms2bot";
    private static final String SEND_USER_HELP = "For sending an sms, please use: send <number> <message>";

    private SmsSender smsSender;

    private final String token;
    private SlackSession session;

    @Inject
    public SlackBotCaller(SmsSender smsSender, @Slack String token) throws Exception {

        this.smsSender = smsSender;
        this.token = token;
        session = SlackSessionFactory.createWebSocketSlackSession(token);
        session.connect();
        session.addMessagePostedListener(this);

    }

    @Override
    public boolean requiresPolling() {
        return false;
    }

    @Override
    public List<String> getSubscribedChannels() {
        return Arrays.asList(CHANNEL_NAME);
    }

    @Override
    public List<SmsMessage> getMessages() throws Exception {
        throw new MethodNotSupportedException("Copoooooonnnnn! I don't manage this kind of calls!!!");
    }

    @Override
    public void sendMessage(String channel, String message) throws Exception {
        SlackChannel slackChannel = session.findChannelByName(channel);
        LOG.info("Sending message [{}] to channel [{}]", message, slackChannel.getName());
        session.sendMessage(slackChannel, message);
    }

    @Override
    public void close() throws IOException {
        session.disconnect();
    }

    @VisibleForTesting
    void sendSms(String messageContent, String messageSender) throws Exception {

        String[] tokens = messageContent.split(" ");

        // Filter wrong number of params: <command> <msisdn> <text_with_spaces>
        if (tokens.length < 3 || !tokens[0].contentEquals("send")) {
            sendMessage(CHANNEL_NAME, SEND_USER_HELP);
            return;
        }

        // Filter wrong destination number
        String destinationNumber = tokens[1];
        if (!isValid(destinationNumber)) {
            sendMessage(CHANNEL_NAME, "@" + messageSender + " : Wrong destination number [" + destinationNumber + "]");
            return;
        }

        // Text to send is everything after the token[2] included
        String textToSend = messageContent.substring(messageContent.indexOf(tokens[2]));
        try {
            String smsId = smsSender.sendSms("origin", destinationNumber, textToSend);
            sendMessage(CHANNEL_NAME, "Sending text message : [" + textToSend + "] to number: [" + destinationNumber + "]. Sms id : [" + smsId +"]");
        } catch (Exception e) {
            sendMessage(CHANNEL_NAME, "Error sending sms: " + e.getMessage());
        }

    }

    @Override
    public void onEvent(SlackMessagePosted event, SlackSession session) {
        try {
            String messageContent = event.getMessageContent();
            String userName = event.getSender().getUserName();
            if (!BOT_NAME.contentEquals(userName)) {
                LOG.info("User [{}] sent message [{}] on channel [{}]", userName, messageContent, event.getChannel());
                sendSms(messageContent, userName);
            }
        } catch (Exception e) {
            LOG.error("Not able to process message received. Exception: ", e);
        }
    }

}
