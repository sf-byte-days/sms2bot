package com.sms2bot.bot;

import com.sms2bot.sms.SmsMessage;

import java.util.List;

public interface BotMessageManager {

    boolean requiresPolling();

    List<String> getSubscribedChannels();

    List<SmsMessage> getMessages() throws Exception;

    void sendMessage(String channel, String text) throws Exception;

}
