package com.sms2bot.bot;

// TODO Maybe move to ConfigUtils
public enum Bot {

    TELEGRAM, SLACK, DUMMY;

    public static Bot fromString(String code) {

        for(Bot bot : Bot.values()) {
            if(bot.toString().equalsIgnoreCase(code)) {
                return bot;
            }
        }

        return null;
    }

}
