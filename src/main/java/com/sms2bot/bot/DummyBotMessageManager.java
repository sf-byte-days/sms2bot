package com.sms2bot.bot;

import com.sms2bot.sms.DummySmsSender;
import com.sms2bot.sms.SmsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

// TODO: Does the name of this needs to change to DummyBotCaller or are the other 2 classes called Caller need to be
// rename to *MessageManager???
public class DummyBotMessageManager implements BotMessageManager {

    private static final Logger LOG = LoggerFactory.getLogger(DummySmsSender.class);

    public DummyBotMessageManager() throws Exception {
        LOG.info("Dummy Bot created");
    }

    @Override
    public boolean requiresPolling() {
        return true;
    }

    @Override
    public List<String> getSubscribedChannels() {
        return Arrays.asList("DUMMY_CHANNEL");
    }

    @Override
    public List<SmsMessage> getMessages() throws Exception {
        return Arrays.asList(new SmsMessage("dest1", "pene"),
                             new SmsMessage("dest2", "culo"),
                             new SmsMessage("dest3", "kk"),
                             new SmsMessage("dest4", "pedo"),
                             new SmsMessage("dest5", "pis"));
    }

    @Override
    public void sendMessage(String chat, String text) {
        LOG.info("Sending msg {} to {}", text, chat);
    }

}
