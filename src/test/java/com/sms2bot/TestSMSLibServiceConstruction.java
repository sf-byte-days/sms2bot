package com.sms2bot;

import com.sms2bot.bot.BotMessageManager;
import com.sms2bot.utils.ConfigUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.smslib.GatewayException;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;

import static com.sms2bot.ModemConfig.*;
import static com.sms2bot.OSUtils.HostOS.Linux;
import static com.sms2bot.OSUtils.HostOS.Windows;
import static com.sms2bot.OSUtils.HostOS.Unknown;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(OSUtils.class)
public class TestSMSLibServiceConstruction {

    @Mock
    private ConfigUtils.BotCommand mockedCommand;

    @Before
    public void setup() {

        initMocks(this);

        mockStatic(OSUtils.class);
        // As service is also a static component inside getSMSLibService, we should clear the registered gateways
        // before each test
        Service.getInstance().getGateways().clear();

    }

    @Test(expected = IllegalStateException.class)
    public void testExceptionIsThrownWhenNoSupportedOSIsDetected() {

        PowerMockito.when(OSUtils.detectHostOS()).thenReturn(Unknown);
        try {
            // Should thrown an Illegal State ex
            new MessageManagerModule(mockedCommand).getSMSLibService(mock(BotMessageManager.class));
        } catch (GatewayException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testWindowsConfigurationIsReturnedProperly() throws Exception {

        PowerMockito.when(OSUtils.detectHostOS()).thenReturn(Windows);
        Service smsLibService = new MessageManagerModule(mockedCommand) .getSMSLibService(mock(BotMessageManager.class));

        // Note how we verify the class, not the instance!
        verify(OSUtils.class);

        SerialModemGateway gateway = (SerialModemGateway) smsLibService.getGateways().iterator().next();
        assertEquals(MODEM_ID, gateway.getGatewayId());
        assertNull(gateway.getSimPin()); // TODO Must be assertEquals(WINDOWS_PIN, gateway.getSimPin());

    }

    @Test
    public void testLinuxConfigurationIsReturnedProperly() throws Exception {

        PowerMockito.when(OSUtils.detectHostOS()).thenReturn(Linux);
        Service smsLibService = new MessageManagerModule(mockedCommand).getSMSLibService(mock(BotMessageManager.class));

        // Note how we verify the class, not the instance!
        verify(OSUtils.class);

        SerialModemGateway gateway = (SerialModemGateway) smsLibService.getGateways().iterator().next();
        assertEquals(MODEM_ID, gateway.getGatewayId());
        assertNull(gateway.getSimPin());

    }

}
