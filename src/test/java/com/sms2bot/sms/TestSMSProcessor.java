package com.sms2bot.sms;

import com.sms2bot.bot.BotMessageManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.smslib.InboundMessage;
import org.smslib.Message;
import org.smslib.modem.ModemGateway;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestSMSProcessor {

    final static String MSISDN = "1111";
    final static String TEXT_TO_SEND = "test_sms";
    
    @Mock
    private BotMessageManager bot;

    @Mock
    private ModemGateway modemGateway;

    @Mock
    private InboundMessage sms;

    @Before
    public void initMocksAndComponents() throws Exception {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testMessageRedirectionToBot() throws Exception {

        final String EXPECTED_TEXT = "Received SMS : [" + TEXT_TO_SEND + "] from number [" + MSISDN + "]";
        final String CHANNEL = "test_channel";

        // Test configuration
        doReturn(TEXT_TO_SEND).when(sms).getText();
        doReturn(MSISDN).when(sms).getOriginator();
        doReturn(Arrays.asList(CHANNEL)).when(bot).getSubscribedChannels();

        // Component under test
        SmsProcessor smsProcessor = new SmsProcessor(bot);

        // Test msg redirection to bot
        smsProcessor.process(modemGateway, Message.MessageTypes.INBOUND, sms);

        ArgumentCaptor<String> textSent = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> channel = ArgumentCaptor.forClass(String.class);
        verify(bot, times(1)).sendMessage(channel.capture(), textSent.capture());
        assertEquals(EXPECTED_TEXT, textSent.getValue());

    }

}
