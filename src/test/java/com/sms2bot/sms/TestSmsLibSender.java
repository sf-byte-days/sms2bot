package com.sms2bot.sms;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.ModemGateway;

import java.util.Arrays;

import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestSmsLibSender {

    @Mock
    private Service smsService;

    @Mock
    private ModemGateway modemGateway;

    @Before
    public void initMocksAndComponents() throws Exception {

        MockitoAnnotations.initMocks(this);

        doReturn(Arrays.asList(modemGateway)).when(smsService).getGateways();

    }

    @Test
    public void testSmsLibSender() throws Exception {

        // Component under test
        SmsLibSender smsLibSender = new SmsLibSender(smsService);

        // Test the SmsLibSender service is initiated properly (the sms service is started)
        smsLibSender.startUp();
        verify(smsService, times(1)).startService();

        // Test an sms message is sent through the underlying library
        ArgumentCaptor<OutboundMessage> outboundMsg = ArgumentCaptor.forClass(OutboundMessage.class);
        smsLibSender.sendSms("origin", "destination", "my_text");
        verify(smsService, times(1)).sendMessage(outboundMsg.capture());
        assertEquals("destination", outboundMsg.getValue().getRecipient());
        assertEquals("my_text", outboundMsg.getValue().getText());

        // Test the SmsLibSender service is stopped properly (the sms service is stopped)
        smsLibSender.shutDown();
        verify(smsService, times(1)).stopService();

    }

}
