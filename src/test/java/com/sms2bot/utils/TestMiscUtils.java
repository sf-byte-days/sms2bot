package com.sms2bot.utils;

import org.junit.Test;

import static com.sms2bot.utils.MiscUtils.isMobilePhone;
import static com.sms2bot.utils.MiscUtils.isShortCode;
import static com.sms2bot.utils.MiscUtils.isValid;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestMiscUtils {

    @Test
    public void testShortCode() {

        assertTrue(isShortCode("000"));
        assertTrue(isShortCode("000000"));
        assertFalse(isShortCode("11"));
        assertFalse(isShortCode("111111111111"));
        assertFalse(isShortCode("+333"));

    }

    @Test
    public void testMsisdn() {

        assertTrue(isMobilePhone("491728389526"));
        assertTrue(isMobilePhone("+491728389526"));
        assertTrue(isMobilePhone("00491728389526"));
        assertFalse(isMobilePhone("1111+1111111"));
        assertFalse(isMobilePhone("491"));
        assertFalse(isMobilePhone("49123456789012345678901234"));

    }

    @Test
    public void testIsValid() {

        assertTrue(isValid("000"));
        assertTrue(isValid("0000000"));
        assertFalse(isShortCode("11"));
        assertFalse(isShortCode("+333"));
        assertTrue(isMobilePhone("491728389526"));
        assertTrue(isMobilePhone("+491728389526"));
        assertTrue(isMobilePhone("00491728389526"));
        assertFalse(isMobilePhone("49123456789012345678901234"));
        assertFalse(isMobilePhone("1111+1111111"));

    }
}
