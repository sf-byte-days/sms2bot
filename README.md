# sms2bot 

This is a standalone application that reads messages from a bot and sends an SMS to a target phone and vice versa, 
reads SMSs and sends messages to the users listening on a bot channel.

[![build status](https://gitlab.com/sf-byte-days/sms2bot/badges/master/build.svg)](https://gitlab.com/sf-byte-days/sms2bot/commits/master)
[![codedecov.io](http://codecov.io/gl/rafatoba/bot-poller/coverage.svg?branch=master)](http://codecov.io/gl/sf-byte-days/sms2bot?branch=master)

## Requirements

A USB modem and a SIM to send SMS over the telephone line. Currently, the [SMSLib](https://github.com/tdelenikas/smslib)
is used for this.

## Supported Bot Platforms

Currently Telegram and Slack are supported.

# License information

MIT (See LICENSE file in the repo)